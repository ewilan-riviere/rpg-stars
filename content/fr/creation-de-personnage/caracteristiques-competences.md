---
title: Caractéristiques et compétences
description: ''
position: 3
category: Création de personnage
---

## Les caractéristiques principales

Un personnage dispose de cinq caractéristiques :

- La **Force** est une mesure de la puissance musculaire du personnage. Cette caractéristique donne une idée de ce qu’il peut soulever, porter, pousser ou tirer.
- La **Dextérité** mesure la vivacité, l’agilité et la rapidité physique du personnage.
- L’**Endurance** rend compte de la vitalité et de la santé du personnage.
- L’**Intelligence** décrit les capacités d’apprentissage, de mémorisation et d’analyse de votre personnage. Elle ne remplace pas l’intelligence du joueur, mais gère, à sa place, un certain nombre de tâches fastidieuses.
- L'**Intuition** décrit la capacité à savoir ce qu'on ne sait pas, comprendre l'incompréhensible et plus généralement à faire preuve d'intuition.

### Répartition

Répartissez **60 points** dans ces 5 caractéristiques avec un *minimum de 4* et un *maximum de 18*.
Une fois les caractéristiques définies, remplissez **les points de vie**, ils sont égaux à *l’Endurance avec un maximum de 14* et la **Santé mentale** qui est égale à l'Intuition avec un maximum de 14.

Au vu de ces statistiques, réfléchissez avec le joueur à la « profession » de son aventurier. *Soldat* ? *Érudit* ? *Marchand* ? *Diplomate* ?

## Les compétences

L’essentiel des actions pouvant échouer des personnages passe par le recours à des compétences. Chacune d’elles s’exprime en pourcentage et suppose de lancer un dé à cent faces (abrégé en 1d100). Si le résultat du dé est inférieur ou égal à la compétence, l’action est réussie.

> *Exemple* : Atlan tente de convaincre un noble de le laisser entrer à une soirée. Atlan a 80 % en Mentir, convaincre et son joueur lance deux dés. Avec un score de 59, Atlan parvient à convaincre le noble de le laisser passer.

### Renseigner ses compétences

Ce sont vos compétences qui vous permetteront de surmonter les épreuves que votre MJ préféré·e mettra sur votre chemin. Vous allez devoir renseigner vos capacités pour personnaliser son héros en développant davantage certaines compétences.

- Pour chaque compétence, ajoutez les deux caractéristiques liées et multipliez le tout par 2. Il s’agit du scoreinitial de la compétence.
- Une fois tous les scores initiaux renseignés, vous disposez de 50 points à répartir comme bon vous semble, sur la base de 1 pour 1. À la création, aucun personnage ne peut disposer d’une compétence supérieure à 90 %.

> *Exemple* : Atlan dispose toujours d’un score de 13 en Charisme et de 10 en Intelligence. Son score initial en Mentir, convaincre est donc de: (13+10) x 2, soit 46 %. Il décide de dépenser 34 de ses 50 points pour améliorer cette compétence qu’il estime vitale, passant son score à 80 %.

### Liste des compétences

TODO

![skills](/images/skills.jpg)

## Compétence spéciale

Chaque personnage dispose d'une compétence spéciale et selon la puissance de la compétence, le·a MJ définira la limite :

- Fréquence, exemple: une seule fois par jour
- Succès, exemple: 50% avec des conséquences en cas d'échec
- Utilisations, exemple: 3 utilisations maximales

Certaines quêtes telles que les quêtes personnelles peuvent donner des capacités spéciales supplémentaires.

## Objet fétiche

Pour vous aider lors de votre voyage dans les profondeurs de l'espace vous emportez un objet fétiche qui que vous pourrez utiliser pour vous rassurer.
